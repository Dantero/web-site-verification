<?php

function generateRandomString($length = 16)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
session_start();

$url = $_POST['website-url'];

$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "database";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM dante_verification WHERE address='". $url ."'";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    echo "Bu website zaten onaylanmış";
    exit;
}

if (!filter_var($url, FILTER_VALIDATE_URL)) {
    echo "Lütfen geçerli bir website adresi girin.";
    exit;
}

$fileName = "dantero" . generateRandomString() . ".html";

$_SESSION['website-url'] = $url;
$_SESSION['file-name'] = $fileName;

// Dosyayı oluştur ve kaydet
$file = fopen($fileName, 'w') or die('Cannot open file.'); //implicitly creates file
$data = "dantero-site-verification: " . $fileName;
fwrite($file, $data);

echo "<script type='text/javascript'> fetch('http://localhost/" . $fileName . "').then(function(t) {";
echo "return t.blob().then((b)=>{";
echo "var a = document.createElement('a');";
echo "a.href = URL.createObjectURL(b);";
echo "a.setAttribute('download', '" . $fileName . "');";
echo "a.click();";
echo "}";
echo ");";
echo "});</script>";

echo "<h2>Lütfen dosyayı websitenize attıkdan sonra onayla butonuna basınız.</h2>";
echo "<form action=\"checkVerification.php\" method=\"post\">";
echo "<p><input type=\"submit\" value=\"Onayla\" /></p>";
echo "</form>";

exit;
