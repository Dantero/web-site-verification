<?php
session_start();
$url = $_SESSION['website-url'];
$fileName = $_SESSION['file-name'];

if (substr($url, -1) !== "/") {
    $url .= "/";
}
$data = file_get_contents($url . $fileName);

if ($data === "dantero-site-verification: " . $fileName) {
    $servername = "localhost";
    $username = "username";
    $password = "password";
    $dbname = "database";

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO `dante_verification` (address, file_name) VALUES ('". $url ."', '". $fileName ."')";

    if ($conn->query($sql) === TRUE) {
        echo "Siteniz başarıyla onaylandı.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
} else {
    echo "Dosya bulunamadı. Lütfen tekrar deneyin.";
}
exit;